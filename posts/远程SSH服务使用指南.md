# 远程SSH服务使用指南

|Author|Email|
|---|---
|Yaoyao Liu|yaoyaoliu@msn.com|

本文所有教程以ubuntu为例，对其他unix内核系统如Debian、CentOS、macOS等也适用。

## 目录
[安装并开启SSH服务](#安装并开启SSH服务)

[使用SSH密钥访问服务器](#使用SSH密钥访问服务器)

[使用SSH反向隧道实现内网穿透](#使用SSH反向隧道实现内网穿透)


### 安装并开启SSH服务
使用SSH访问远程服务器，需要在服务器端安装并开启SSH服务。
安装openssh
```sh
(at server) sudo apt-get install openssh-server
```
查看ssh状态
```sh
(at server) ps -aux|grep ssh
```
启动服务
```sh
(at server) service ssh start
```
假设服务器相关信息如下：
```
host: 123.123.123.123
username: example-user
```
那么我们就可以用如下方式远程访问服务器
```sh
(at local) ssh example-user@123.123.123.123
```
### 使用SSH密钥访问服务器
在本地生成RSA密钥和公钥，```-t``` 表示type，就是说要生成RSA加密的钥匙
```sh
(at local) ssh-keygen -t rsa
```
ssh密钥默认的保存路径是```~/.ssh/```，文件名为```id_rsa```（私钥）和```id_rsa.pub```(公钥)
然后，你需要将ssh密钥上传到服务器端，可以用如下命令快速实现这个操作
```sh
(at local) ssh-copy-id example-user@123.123.123.123
```
也可以手动完成：
```sh
(at local) cat ~/.ssh/id_rsa.pub | ssh example-user@123.123.123.123 'cat - >> ~/.ssh/authorized_keys'
```
```sh
(at server) sudo chmod 600 ~/.ssh/authorized_keys
```
这样，我们就可以使用密钥进行免密码SSH远程访问了。
### 设置SSH别名登陆
使用vim编辑如下文件
```sh
(at local) vim ~/.ssh/config
```
在该文件中添加如下的内容
```
Host example-server
HostName 123.123.123.123
User example-user
IdentityFile ~/.ssh/id_rsa
```
这样，我们就可以通过别名快速访问远程服务器了
```sh
(at local) ssh example-server
```
### 使用SSH反向隧道实现内网穿透
有的时候，我们的服务器可能会位于NAT之后，通过路由器连接互联网，没有公网IP地址。
这样，我们就没有办法直接使用如上的方式进行SSH了。
通过云服务器建立SSH反向隧道，进行端口转发，可以很好地解决这个问题。
#### 服务器配置

|代号|公网IP|用户名|备注|
|---|---|---|---|
|local|-|-|本地主机|
|server|无|example-user|目标服务器，无公网IP地址|
|cloud|123.123.255.255|cloud-user|用公网IP的主机，一般使用云服务器|

#### 使用到的SSH参数
反向代理 ```ssh -fCNR```
正向代理 ```ssh -fCNL```
参数解释：
```
-f 后台执行ssh指令
-C 允许压缩数据
-N 不执行远程指令
-R 将远程主机(服务器)的某个端口转发到本地端指定机器的指定端口
-L 将本地机(客户机)的某个端口转发到远端指定机器的指定端口
-p 指定远程主机的端口
```
#### 在每个终端上进行的操作
在目标服务器上执行：
```sh
(at server) autossh -p 22 -M 6777 -NR '*:6766:localhost:22' cloud-user@123.123.255.255 -f
```
在云服务器上修改如下文件
```sh
(at cloud) sudo vim /etc/ssh/sshd_config
```
将```GatewayPorts```改为```yes```
在本地主机上，就可以用如下方式访问目标服务器
```sh
(at local) ssh -p 6766 example-user@123.123.255.255
```
[源文件](https://yaoyao-liu.com/markdown-archive/)

参考文献

[ukeyim: ssh反向隧道实现端口转发(需要公网server)](https://www.jianshu.com/p/555c74a60883)

[Linux魔法师: SSH无密码登录：只需两个简单步骤 (Linux)](https://www.linuxdashen.com/ssh-key%EF%BC%9A%E4%B8%A4%E4%B8%AA%E7%AE%80%E5%8D%95%E6%AD%A5%E9%AA%A4%E5%AE%9E%E7%8E%B0ssh%E6%97%A0%E5%AF%86%E7%A0%81%E7%99%BB%E5%BD%95)

[kelvv: 忘记ip地址，用ssh别名登陆](https://www.jianshu.com/p/5729d5e61c72)


