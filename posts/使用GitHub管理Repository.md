# 使用GitHub管理Repository

### 对已有的Repository进行修改
将已有的项目克隆到本地
```bash
git clone https://github.com/username/project-name
```
或者同步已经下载的项目
```bash
git pull origin master
```
前往项目所在的路径
```bash
cd project-name
```
进行修改后提交
```bash
git add .
git commit -m commit comment
git push -u origin master
```
### 在本地创建新的Repository
在项目目录下，执行初始化
```bash
git init
```
然后添加文件到git
```bash
git add .
git commit -m 'commit comments'
```
将本地的仓库关联到github上
```bash
git remote add origin git@github.com:https://github.com/username/project-name.git
```
上传github之前，要先pull一下
```bash
git pull origin master
```
然后再进行上传
```bash
git push -u origin master
```

参考文献
[三只要有你: 如何上传本地代码到github](https://zhuanlan.zhihu.com/p/34625448)

